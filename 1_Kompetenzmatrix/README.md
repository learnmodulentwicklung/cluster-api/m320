# Kompetenzmatrix - Modul 000

| Kompetenzband: | HZ | Grundlagen | Fortgeschritten | Erweitert |
| --------------------- | --------------------------- | ------------------ | ------------------ | ------------------ |
| Band A | a | AG1:                  | AF1:                        | AE1:               |
| Band B | b | BG1:                  | BF1:                        | BE1:               |
|                   |       b,c            | BG2:                 | BF2:                       | BE2:              |
| Band C | b,c | CG1: | CF1: | CE1: |
| Band D | d |  | DF1: | DE1: |
| Band E | d | EG1: | EF1: | EE1: |
|  | d,e | EG2: | EF2: | EE2: |
|  | d | EG3: | EF3: |  |

## Kompetenzstufen

### Grundlagen | Der Anfänger | Stufe 1 

Diese Stufe ist als Einstieg ins Thema gedacht. Der Fokus liegt hier auf dem Verstehen von Begriffen und Zusammenhängen. 

*Als Richtungshinweis: Wer alle Kompetenzen in dieser Stufe erfüllt, hat die Noten 3.5.*

### Fortgeschritten | Der Kompetente | Stufe 2 

Diese Stufe definiert den Pflichtstoff, den alle Lernenden am Ende des Moduls möglichst beherrschen sollen.  

*Als Richtungshinweis: Wer alle Kompetenzen in dieser Stufe erfüllt, hat die Noten 4.75*

### Erweitert | Der Gewandete | Stufe 3 

Diese Lerninhalte für Lernende gedacht, die schneller vorankommen und einen zusätzlichen Lernanreiz erhalten sollen.  

*Als Richtungshinweis: Wer alle Kompetenzen in dieser Stufe erfüllt, hat die Noten 6*

## Fragekatalog

Link zum [Fragekatalog](Fragenkatalog.md)
